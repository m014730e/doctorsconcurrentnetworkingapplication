﻿namespace ServerForm
{
    partial class Server
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Server));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.doctorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overallToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label_main = new System.Windows.Forms.Label();
            this.richTextBox_convo = new System.Windows.Forms.RichTextBox();
            this.label_patient_name = new System.Windows.Forms.Label();
            this.label_app_time = new System.Windows.Forms.Label();
            this.richTextBox_names = new System.Windows.Forms.RichTextBox();
            this.richTextBox_times = new System.Windows.Forms.RichTextBox();
            this.richTextBox_overall = new System.Windows.Forms.RichTextBox();
            this.button_clear_overall = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.doctorToolStripMenuItem,
            this.overallToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(486, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // doctorToolStripMenuItem
            // 
            this.doctorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusToolStripMenuItem,
            this.logsToolStripMenuItem});
            this.doctorToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doctorToolStripMenuItem.Name = "doctorToolStripMenuItem";
            this.doctorToolStripMenuItem.Size = new System.Drawing.Size(81, 29);
            this.doctorToolStripMenuItem.Text = "Doctor";
            // 
            // statusToolStripMenuItem
            // 
            this.statusToolStripMenuItem.Name = "statusToolStripMenuItem";
            this.statusToolStripMenuItem.Size = new System.Drawing.Size(181, 30);
            this.statusToolStripMenuItem.Text = "Status";
            this.statusToolStripMenuItem.Click += new System.EventHandler(this.statusToolStripMenuItem_Click);
            // 
            // logsToolStripMenuItem
            // 
            this.logsToolStripMenuItem.Name = "logsToolStripMenuItem";
            this.logsToolStripMenuItem.Size = new System.Drawing.Size(181, 30);
            this.logsToolStripMenuItem.Text = "Logs";
            this.logsToolStripMenuItem.Click += new System.EventHandler(this.logsToolStripMenuItem_Click);
            // 
            // overallToolStripMenuItem
            // 
            this.overallToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.overallToolStripMenuItem.Name = "overallToolStripMenuItem";
            this.overallToolStripMenuItem.Size = new System.Drawing.Size(84, 29);
            this.overallToolStripMenuItem.Text = "Overall";
            this.overallToolStripMenuItem.Click += new System.EventHandler(this.overallToolStripMenuItem_Click);
            // 
            // label_main
            // 
            this.label_main.AutoSize = true;
            this.label_main.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_main.Location = new System.Drawing.Point(12, 57);
            this.label_main.Name = "label_main";
            this.label_main.Size = new System.Drawing.Size(0, 25);
            this.label_main.TabIndex = 1;
            // 
            // richTextBox_convo
            // 
            this.richTextBox_convo.BackColor = System.Drawing.Color.White;
            this.richTextBox_convo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_convo.Location = new System.Drawing.Point(17, 104);
            this.richTextBox_convo.Name = "richTextBox_convo";
            this.richTextBox_convo.ReadOnly = true;
            this.richTextBox_convo.Size = new System.Drawing.Size(456, 250);
            this.richTextBox_convo.TabIndex = 2;
            this.richTextBox_convo.Text = "";
            this.richTextBox_convo.Visible = false;
            // 
            // label_patient_name
            // 
            this.label_patient_name.AutoSize = true;
            this.label_patient_name.BackColor = System.Drawing.SystemColors.Control;
            this.label_patient_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_patient_name.Location = new System.Drawing.Point(40, 107);
            this.label_patient_name.Name = "label_patient_name";
            this.label_patient_name.Size = new System.Drawing.Size(110, 20);
            this.label_patient_name.TabIndex = 3;
            this.label_patient_name.Text = "Patient Name";
            this.label_patient_name.Visible = false;
            // 
            // label_app_time
            // 
            this.label_app_time.AutoSize = true;
            this.label_app_time.BackColor = System.Drawing.SystemColors.Control;
            this.label_app_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_app_time.Location = new System.Drawing.Point(251, 107);
            this.label_app_time.Name = "label_app_time";
            this.label_app_time.Size = new System.Drawing.Size(144, 20);
            this.label_app_time.TabIndex = 4;
            this.label_app_time.Text = "Appointment Time";
            this.label_app_time.Visible = false;
            // 
            // richTextBox_names
            // 
            this.richTextBox_names.BackColor = System.Drawing.Color.White;
            this.richTextBox_names.Location = new System.Drawing.Point(34, 144);
            this.richTextBox_names.Name = "richTextBox_names";
            this.richTextBox_names.Size = new System.Drawing.Size(169, 210);
            this.richTextBox_names.TabIndex = 5;
            this.richTextBox_names.Text = "";
            this.richTextBox_names.Visible = false;
            // 
            // richTextBox_times
            // 
            this.richTextBox_times.BackColor = System.Drawing.Color.White;
            this.richTextBox_times.Location = new System.Drawing.Point(255, 144);
            this.richTextBox_times.Name = "richTextBox_times";
            this.richTextBox_times.Size = new System.Drawing.Size(169, 210);
            this.richTextBox_times.TabIndex = 6;
            this.richTextBox_times.Text = "";
            this.richTextBox_times.Visible = false;
            // 
            // richTextBox_overall
            // 
            this.richTextBox_overall.BackColor = System.Drawing.Color.White;
            this.richTextBox_overall.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_overall.Location = new System.Drawing.Point(18, 107);
            this.richTextBox_overall.Name = "richTextBox_overall";
            this.richTextBox_overall.Size = new System.Drawing.Size(456, 250);
            this.richTextBox_overall.TabIndex = 7;
            this.richTextBox_overall.Text = "";
            this.richTextBox_overall.Visible = false;
            // 
            // button_clear_overall
            // 
            this.button_clear_overall.AutoSize = true;
            this.button_clear_overall.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_clear_overall.Location = new System.Drawing.Point(378, 41);
            this.button_clear_overall.Name = "button_clear_overall";
            this.button_clear_overall.Size = new System.Drawing.Size(96, 41);
            this.button_clear_overall.TabIndex = 8;
            this.button_clear_overall.Text = "Clear ";
            this.button_clear_overall.UseVisualStyleBackColor = true;
            this.button_clear_overall.Visible = false;
            this.button_clear_overall.Click += new System.EventHandler(this.button_clear_overall_Click);
            // 
            // Server
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 394);
            this.Controls.Add(this.button_clear_overall);
            this.Controls.Add(this.richTextBox_overall);
            this.Controls.Add(this.richTextBox_times);
            this.Controls.Add(this.richTextBox_names);
            this.Controls.Add(this.label_app_time);
            this.Controls.Add(this.label_patient_name);
            this.Controls.Add(this.richTextBox_convo);
            this.Controls.Add(this.label_main);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Server";
            this.Text = "Staffordshire Doctors Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Server_FormClosing);
            this.Load += new System.EventHandler(this.Server_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem doctorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem overallToolStripMenuItem;
        private System.Windows.Forms.Label label_main;
        private System.Windows.Forms.RichTextBox richTextBox_convo;
        private System.Windows.Forms.Label label_patient_name;
        private System.Windows.Forms.Label label_app_time;
        private System.Windows.Forms.RichTextBox richTextBox_names;
        private System.Windows.Forms.RichTextBox richTextBox_times;
        private System.Windows.Forms.RichTextBox richTextBox_overall;
        private System.Windows.Forms.Button button_clear_overall;
    }
}

