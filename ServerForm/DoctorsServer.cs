﻿using SharedPackets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

namespace ServerForm
{
    class DoctorsServer
    {
        TcpListener                     tcpListener;
        static List<Client>             patients = new List<Client>();
        static BinaryFormatter          bf;
        static System.Timers.Timer      seatTimer, moveToWaitPanelTimer;
        private static Thread           appointmentThread;
        private static string[]         patientNames;
        private static bool             isSeeingPatient;
        private static int              conversationPoint = 0;
        private static List<string[]>   waitList = new List<string[]>();
        private static bool             waitPanelFree = true;
        private static bool             isDeclined;
        private static string           currentPatientName;
        private static RichTextBox      richTextBox_convo, richTextBox_names, richTextBox_times, richTextBox_overall;

        public DoctorsServer(string ipAddress,int port,
                             RichTextBox convo, RichTextBox names,
                             RichTextBox times, RichTextBox overall)
        {
            IPAddress ip = IPAddress.Parse(ipAddress);
            tcpListener = new TcpListener(ip, port);
            bf = new BinaryFormatter();
            richTextBox_convo = convo;
            richTextBox_names = names;
            richTextBox_times = times;
            richTextBox_overall = overall;
            patientNames = new string[5];
            for (int i = 0; i < patientNames.Length; i++)
            {
                patientNames[i] = "";
            }
            moveToWaitPanelTimer = new System.Timers.Timer(500);
            moveToWaitPanelTimer.Elapsed += new ElapsedEventHandler(MovePatientToWaitPanel);
            moveToWaitPanelTimer.Enabled = true;
            
            seatTimer = new System.Timers.Timer(4000);
            seatTimer.Elapsed += new ElapsedEventHandler(TakeForAppointment);
         }
        public void Start()
        {
            tcpListener.Start();
            if (!seatTimer.Enabled)
            {
                seatTimer.Start();
            }
            while (true)
            {
                Socket socket = tcpListener.AcceptSocket();
                Client patient = new Client(socket);
                patients.Add(patient);
                patient.Start();
            }
        }
        public void Stop()
        {
            foreach (Client patient in patients)
            {
                tcpListener.Stop();
            }
        }
        public static void SocketMethod(Client patient)
        {
            try
            {
                Deserialize(patient);
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            finally
            {
                patient.Stop();
            }
        }
        public static void Deserialize(Client patient)
        {
            ClientIDPacket clientIDPacket = new ClientIDPacket(patient.ID, patientNames);
            patient.SendPacket(clientIDPacket);

            int noOfBytes;
            while ((noOfBytes = patient.reader.ReadInt32()) != 0)
            {
                byte[] bytes = patient.reader.ReadBytes(noOfBytes);
                MemoryStream memory = new MemoryStream(bytes);
                Packet packet = bf.Deserialize(memory) as Packet;

                switch (packet.type)
                {
                    case PacketType.ADD_TO_WAITLIST:
                        PrintToOverall("Patient: " + ((AddToWaitListPacket)packet).id_name[1] + " added to waiting list");
                        waitList.Add(((AddToWaitListPacket)packet).id_name);
                        break;

                    case PacketType.ENTRY_PANEL_SIGNAL:
                        foreach (Client p in patients)
                        {
                            p.SendPacket(packet);
                        }
                        break;

                    case PacketType.REQUEST_SEAT:
                        string patientName = ((RequestSeatPacket)packet).patientName;
                        string patientColour = ((RequestSeatPacket)packet).patient_Colour;
                        string patientID = ((RequestSeatPacket)packet).client_ID;
                        bool result = SeatsAvailable();
                        if (result == true)
                        {
                            TakeNextAvailableSeat(((RequestSeatPacket)packet).patientName);
                        }
                        else
                        {
                            PrintToOverall("Patient: " + ((RequestSeatPacket)packet).patientName + " waiting for an available seat");
                            isDeclined = true;
                        }
                        SuccessSitPacket successSitPacket = new SuccessSitPacket(patientNames);

                        foreach (Client c in patients)
                        {
                            if (result == true)
                            {
                                c.SendPacket(successSitPacket);
                                waitPanelFree = true;
                            }
                        }
                        break;
                        
                    case PacketType.CLIENT_DISCONNECT:
                        object clientLock = new object();
                        lock(clientLock)
                        {
                            for (int clientIndex = 0; clientIndex < patients.Count; clientIndex++)
                            {
                                if (patients[clientIndex].ID == ((ClientDisconnectPacket)packet).client_ID)
                                {
                                    patients.RemoveAt(clientIndex);
                                    break;
                                }
                            }
                            if (patients.Count > 0)
                            {
                                for (int clientNum = 0; clientNum < waitList.Count; clientNum++)
                                {
                                    if (waitList[clientNum][0] == ((ClientDisconnectPacket)packet).client_ID)
                                    {
                                        waitList[clientNum][0] = patients[0].ID;
                                    }
                                }
                            }
                            else
                            {
                                //avoids patients in the entry panel going to the wait panel if they're disconnect.
                                waitList.Clear();
                                waitPanelFree = true;
                            }
                            foreach (Client p in patients)
                            {
                                p.SendPacket(new TransferClientIDPacket(((ClientDisconnectPacket)packet).client_ID, patients[0].ID));
                            }
                        }
                        break;
                }
            }
        }
        static object patientLock = new object();
        public static void TakeNextAvailableSeat(string patientName)
        {
            lock (patientLock)
            {
                for (int patientIndex = 0; patientIndex < patientNames.Length; patientIndex++)
                {
                    if (String.IsNullOrEmpty(patientNames[patientIndex]))
                    {
                        patientNames[patientIndex] = patientName;
                        PrintToOverall("Patient: "  + patientName + " took a seat");
                        break;
                    }
                }
            }
        }

        public static bool SeatsAvailable()
        {
            for (int patientIndex = 0; patientIndex < patientNames.Length; patientIndex++)
            {
                if (String.IsNullOrEmpty(patientNames[patientIndex]))
                {
                    return true;
                }
            }
            return false;
        }

        public static void TakeForAppointment(object sender, ElapsedEventArgs e)
        {
            if (!isSeeingPatient)
            {
                lock (patientLock)
                {
                    for (int patientIndex = 0; patientIndex < patientNames.Length; patientIndex++)
                    {
                        if (!String.IsNullOrEmpty(patientNames[patientIndex]))
                        {
                            currentPatientName = patientNames[patientIndex];
                            patientNames[patientIndex] = "";
                            isSeeingPatient = true;
                            PrintToOverall("Patient: " + currentPatientName + " is being seen by the doctor");
                            foreach (Client c in patients)
                            {
                                c.SendPacket(new TakenAppointPacket(patientNames));
                                //send a packet to every client to retry
                                if (isDeclined)
                                {
                                    c.SendPacket(new TryAgainPacket());
                                }
                            }
                            appointmentThread = new Thread(new ThreadStart(StartAppointment));
                            appointmentThread.Start();

                            if (isDeclined)
                            {
                                isDeclined = false;
                            }
                            seatTimer.Stop();
                            break;
                        }
                    }
                }
            }
        }
        public static void StartAppointment()
        {
            System.Timers.Timer appointmentTimer = new System.Timers.Timer(1000);
            appointmentTimer.Elapsed += new ElapsedEventHandler(Appointment);
            appointmentTimer.Enabled = true;

        }
        private delegate void PrintToOverallDelegate(string message);
        public static void PrintToOverall(string message)
        {
            if (richTextBox_overall.InvokeRequired)
            {
                richTextBox_overall.Invoke(new PrintToOverallDelegate(PrintToOverall), new object[] { message });
            }
            else
            {
                richTextBox_overall.AppendText(message + "\n");
            }
        }
        private delegate void AppointmentDelegate(object sender, ElapsedEventArgs e);
        public static void Appointment(object sender, ElapsedEventArgs e)
        {
            if (richTextBox_convo.InvokeRequired)
            {
                richTextBox_convo.Invoke(new AppointmentDelegate(Appointment), new object[] { sender, e });
            }
            else
            {
                DateTime now = DateTime.UtcNow;
                switch (conversationPoint)
                {
                    case 0:
                        richTextBox_convo.Clear();
                        richTextBox_convo.AppendText("Hello, doctor...\n");
                        break;
                    case 1:
                        richTextBox_convo.AppendText("Hello, patient...\n");
                        break;
                    case 2:
                        richTextBox_convo.AppendText("What is wrong?\n");
                        break;
                    case 3:
                        richTextBox_convo.AppendText("I think I have...\n");
                        break;
                    case 4:
                        richTextBox_convo.AppendText("Okay, here is your medication...\n");
                        break;
                    case 5:
                        richTextBox_convo.AppendText("Thanks, doctor. Bye!...\n");
                        break;
                    default:
                        break;
                }
                conversationPoint++;

                if (conversationPoint == 6)
                {
                    System.Timers.Timer sender1 = (System.Timers.Timer)sender;
                    sender1.Dispose();
                    isSeeingPatient = false;
                    conversationPoint = 0;
                    foreach (Client p in patients)
                    {
                        p.SendPacket(new LeavePacket("black", currentPatientName));
                    }
                    seatTimer.Start();
                    //show on the log form 
                    richTextBox_names.AppendText("Patient: " + currentPatientName + "\n");
                    richTextBox_times.AppendText("Time: " + string.Format("{0:hh:mm:ss tt}", now) + "\n");
                    PrintToOverall("Patient: " + currentPatientName + " is leaving");
                }
            }
        }

        public void CloseAllClients()
        {
            foreach (Client c in patients)
            {
                c.SendPacket(new ServerDisconnectPacket());
            }
        }

        public void MovePatientToWaitPanel(object sender, ElapsedEventArgs e)
        {
            if (waitPanelFree)
            {
                if (waitList.Count > 0)
                {
                    waitPanelFree = false;
                    string[] tempWaitListIdName = waitList[0];
                    waitList.RemoveAt(0);
                    //get the name
                    PrintToOverall("Patient: " + tempWaitListIdName[1] + " is next to enter the room");
                    foreach (Client p in patients)
                    {
                        p.SendPacket(new MoveToWaitPanelPacket(tempWaitListIdName));
                        if (p.ID == tempWaitListIdName[0])
                        {
                            p.SendPacket(new EntryPanelSignalPacket());
                        }
                    }
                }
            }
        }
    }
}
