﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace ServerForm
{
    public partial class Server : Form
    {
        private Thread serverThread;
        DoctorsServer server;
        public Server()
        {
            InitializeComponent();
            serverThread = new Thread(new ThreadStart(StartServer));
            serverThread.Start();
         }

        private void StartServer()
        {
            server = new DoctorsServer("127.0.0.1", 4444,
                                                    richTextBox_convo,
                                                    richTextBox_names,
                                                    richTextBox_times,
                                                    richTextBox_overall);
            server.Start();
            server.Stop();
        }

        private void statusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label_main.Text = "Status";
            richTextBox_convo.Visible = true;            
            richTextBox_names.Visible = false;
            richTextBox_times.Visible = false;
            label_patient_name.Visible = false;
            label_app_time.Visible = false;
            richTextBox_overall.Visible = false;
            button_clear_overall.Visible = false;
        }

        private void logsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label_main.Text = "Logs";
            richTextBox_convo.Visible = false;
            label_patient_name.Visible = true;
            label_app_time.Visible = true;
            richTextBox_names.Visible = true;
            richTextBox_times.Visible = true;
            richTextBox_overall.Visible = false;
            button_clear_overall.Visible = false;
        }

        private void overallToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label_main.Text = "Overall";
            richTextBox_convo.Visible = false;
            label_patient_name.Visible = false;
            label_app_time.Visible = false;
            richTextBox_names.Visible = false;
            richTextBox_times.Visible = false;
            richTextBox_overall.Visible = true;
            button_clear_overall.Visible = true;
        }

        private void button_clear_overall_Click(object sender, EventArgs e)
        {
            richTextBox_overall.Clear();
        }

        private void Server_FormClosing(object sender, FormClosingEventArgs e)
        {
            server.CloseAllClients();
            Environment.Exit(Environment.ExitCode);
        }

        private void Server_Load(object sender, EventArgs e)
        {
            label_main.Text = "Overall";
            richTextBox_convo.Visible = false;
            label_patient_name.Visible = false;
            label_app_time.Visible = false;
            richTextBox_names.Visible = false;
            richTextBox_times.Visible = false;
            richTextBox_overall.Visible = true;
            button_clear_overall.Visible = true;
        }
    }
}
