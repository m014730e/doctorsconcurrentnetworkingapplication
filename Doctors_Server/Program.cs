﻿using System.Text;

namespace Doctors_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            DoctorsServer server = new DoctorsServer("127.0.0.1", 4444);
            server.Start();
            server.Stop();
        }
    }
}
