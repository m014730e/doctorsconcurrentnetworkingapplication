﻿using SharedPackets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Timers;

namespace Doctors_Server
{
    class DoctorsServer
    {
        TcpListener                     tcpListener;
        static  List<Client>            patients = new List<Client>();
        static  BinaryFormatter         bf;
        static  System.Timers.Timer     seatTimer, moveToWaitPanelTimer;
        private static Thread           appointmentThread;
        private static string[]         patientNames;
        private static bool             isSeeingPatient;
        private static int              conversationPoint = 0;
        private static List<string[]>   waitList = new List<string[]>();
        private static bool             waitPanelFree = true;
        private static bool             isDeclined;
        private static string           currentPatientName;

        public DoctorsServer(string ipAddress, int port)
        {
            IPAddress ip = IPAddress.Parse(ipAddress);
            tcpListener = new TcpListener(ip, port);
            bf = new BinaryFormatter();
            patientNames = new string[5];
            for (int i = 0; i < patientNames.Length; i++)
            {
                patientNames[i] = "";
            }
            seatTimer = new System.Timers.Timer(5000);
            seatTimer.Elapsed += new ElapsedEventHandler(takeForAppointment);

            moveToWaitPanelTimer = new System.Timers.Timer(800);
            moveToWaitPanelTimer.Elapsed += new ElapsedEventHandler(movePatientToWaitPanel);
            moveToWaitPanelTimer.Enabled = true;
        }
        public void Start()
        {
            tcpListener.Start();
            Console.WriteLine("Listening..");
            if (!seatTimer.Enabled)
            {
                seatTimer.Start();
            }
            while (true)
            {
                Socket socket = tcpListener.AcceptSocket();
                Client patient = new Client(socket);
                patients.Add(patient);
                patient.Start();
            }
        }
        public void Stop()
        {
            foreach (Client patient in patients)
            {
                tcpListener.Stop();
            }
        }
        public static void SocketMethod(Client patient)
        {
            try
            {
                Deserialize(patient);
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            finally
            {
                patient.Stop();
            }
        }
        public static void Deserialize(Client patient)
        {

            ClientIDPacket clientIDPacket = new ClientIDPacket(patient.ID, patientNames);
            patient.Send(clientIDPacket);

            int noOfBytes;
            while ((noOfBytes = patient.reader.ReadInt32()) != 0)
            {
                byte[] bytes = patient.reader.ReadBytes(noOfBytes);
                MemoryStream memory = new MemoryStream(bytes);
                Packet packet = bf.Deserialize(memory) as Packet;

                switch (packet.type)
                {
                    case PacketType.ENTRY_PANEL_SIGNAL:
                        foreach (Client p in patients)
                        {
                            p.SendPacket(packet);        
                        }
                        break;
                    case PacketType.REQUEST_SEAT:
                        string patientName = ((RequestSeatPacket)packet).patientName;
                        string patientColour = ((RequestSeatPacket)packet).patient_Colour;
                        string patientID = ((RequestSeatPacket)packet).client_ID;
                        bool result = seatsAvailable();
                        if (result == true)
                        {
                            takeNextAvailableSeat(((RequestSeatPacket)packet).patientName);
                        }
                        else
                        {
                            isDeclined = true;
                        }
                        SuccessSitPacket successSitPacket = new SuccessSitPacket(patientNames);
                        
                        foreach (Client c in patients)
                        {
                            if (result == true)
                            {
                                c.SendPacket(successSitPacket);
                                waitPanelFree = true;
                            }
                        }
                        break;
                    case PacketType.PATIENT_ENTER:
                        Console.WriteLine("Patient entered");
                        foreach (Client p in patients)
                        {
                            p.SendPacket(packet);
                        }
                        break;
                    case PacketType.ADD_TO_WAITLIST:
                        Console.WriteLine("Patient = " + ((AddToWaitListPacket)packet).id_name[1] + " added to list");
                        waitList.Add(((AddToWaitListPacket)packet).id_name);
                        break;
                }
            }
        }
        static object patientLock = new object();
        public static void takeNextAvailableSeat(string patientName)
        {
            lock (patientLock)
            {
                for (int patientIndex = 0; patientIndex < patientNames.Length; patientIndex++)
                {
                    if (String.IsNullOrEmpty(patientNames[patientIndex]))
                    {
                        patientNames[patientIndex] = patientName;
                        break;
                    }
                }
            }
        }

        public static bool seatsAvailable()
        {
            for (int patientIndex = 0; patientIndex < patientNames.Length; patientIndex++)
            {
                if (String.IsNullOrEmpty(patientNames[patientIndex]))
                {
                    return true;
                }
            }
            return false;
        }

        public static void takeForAppointment(object sender, ElapsedEventArgs e)
        {
            if (!isSeeingPatient)
            {
                lock (patientLock)
                {
                    for (int patientIndex = 0; patientIndex < patientNames.Length; patientIndex++)
                    {
                        if (!String.IsNullOrEmpty(patientNames[patientIndex]))
                        {
                            Console.WriteLine("Seeing " + patientNames[patientIndex]);
                            currentPatientName = patientNames[patientIndex];
                            patientNames[patientIndex] = "";
                            isSeeingPatient = true;
                            foreach (Client c in patients)
                            {
                                c.SendPacket(new TakenAppointPacket(patientNames));
                                //send a packet to every client to retry
                                if (isDeclined)
                                {
                                    c.SendPacket(new TryAgainPacket());
                                }
                            }
                            appointmentThread = new Thread(new ThreadStart(StartAppointment));
                            appointmentThread.Start();

                            if (isDeclined)
                            {
                                isDeclined = false;
                            }
                            seatTimer.Stop();
                            break;
                        }
                    }
                }
            }
        }
        public static void StartAppointment()
        {
            System.Timers.Timer appointmentTimer = new System.Timers.Timer(1000);
            appointmentTimer.Elapsed += new ElapsedEventHandler(Appointment);
            appointmentTimer.Enabled = true;

        }
        public static void Appointment(object sender, ElapsedEventArgs e)
        {
            
            switch (conversationPoint)
            { 
                case 0:
                    Console.WriteLine("Hello, doctor...");
                    break;
                case 1:
                    Console.WriteLine("Hello, patient...");
                    break;
                case 2:
                    Console.WriteLine("What is wrong?");
                    break;
                case 3:
                    Console.WriteLine("I think I have...");
                    break;
                case 4:
                    Console.WriteLine("Okay, here is your medication...");
                    break;
                case 5:
                    Console.WriteLine("Thanks, doctor. Bye!...");
                    break;
                default:
                    break;
            }
            conversationPoint++;

            if (conversationPoint == 6)
            {
                System.Timers.Timer sender1 = (System.Timers.Timer)sender;
                sender1.Dispose();
                isSeeingPatient = false;
                conversationPoint = 0;
                foreach (Client p in patients)
                {
                    p.SendPacket(new LeavePacket("black",currentPatientName));
                }
                seatTimer.Start();
            }
        }
        public void movePatientToWaitPanel(object sender, ElapsedEventArgs e)
        {
            if (waitPanelFree)
            {
                if (waitList.Count > 0)
                {
                    waitPanelFree = false;
                    string[] tempWaitListIdName = waitList[0];
                    waitList.RemoveAt(0);
                    foreach (Client p in patients)
                    {
                        p.Send(new MoveToWaitPanelPacket(tempWaitListIdName));
                        if (p.ID == tempWaitListIdName[0])
                        {
                            p.Send(new EntryPanelSignalPacket());
                        }
                    }
                }
            }
        }
    }
}
