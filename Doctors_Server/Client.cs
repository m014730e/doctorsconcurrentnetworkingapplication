﻿using SharedPackets;
using System;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
//using Shared;

namespace Doctors_Server
{
    class Client
    {
        public Socket Socket;
        public string ID;
        private Thread thread;
        public NetworkStream stream;
        public BinaryReader reader;
        public BinaryWriter writer;
        public BinaryFormatter bf;

        public Client(Socket socket)
        {
            this.ID = Guid.NewGuid().ToString();
            this.Socket = socket;
            this.stream = new NetworkStream(socket, true);
            this.reader = new BinaryReader(stream, Encoding.UTF8);
            this.writer = new BinaryWriter(stream, Encoding.UTF8);
        }
        public void Start()
        {
            thread = new Thread(new ThreadStart(SocketMethod));
            thread.Start();
        }
        public void Stop()
        {
            Socket.Close();
            if (thread.IsAlive)
            {
                thread.Abort();
            }
        }
        public void SocketMethod()
        {
            DoctorsServer.SocketMethod(this);
        }

        public void SendPacket(Packet packet)
        {
            if (!Socket.Connected)
                return;

            Send(packet);
        }

        public void Send(Packet data)
        {
            MemoryStream memory = new MemoryStream();
            bf = new BinaryFormatter();
            bf.Serialize(memory, data);
            byte[] buffer = memory.GetBuffer();
            this.writer = new BinaryWriter(stream, Encoding.UTF8);
            this.writer.Write(buffer.Length);
            this.writer.Write(buffer);
            this.writer.Flush();
        }
    }
}
