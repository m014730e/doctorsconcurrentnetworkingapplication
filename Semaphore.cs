﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Assignment2
{
    public class Semaphore
    {
        private int patientCount = 0;

        public void Wait()
        {
            lock (this)
            {
                while (patientCount == 0)
                {
                    Monitor.Wait(this);
                }
                patientCount = 0;
            }
        }

        public void Signal()
        {
            lock (this)
            {
                patientCount = 1;
                Monitor.Pulse(this);
            }
        }
    }
}
