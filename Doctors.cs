﻿using SharedPackets;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Assignment2
{
    public partial class Doctors : Form
    {
        private     EntryPanelThread    entryPanel ;
        public      EntryPanelThread    EntryPanel { get { return this.entryPanel; } }
        private     WaitingPanelThread  waitPanel, exitPanel;
        public      WaitingPanelThread  WaitPanel { get { return this.waitPanel; } }
        public      WaitingPanelThread  ExitPanel { get { return this.exitPanel; } }
        public      Thread              entryThread, takeSeatThread, exitThread;
        public      Semaphore           entrySeatsSemaphore,allowedToWriteToBufferSemaphore, exitSemaphore;
        private     Buffer              entryBuffer, exitBuffer;
        private     bool                connected = false;
        public      DoctorsConnection   connection = null;
        public      Button              Connection_Button { get { return connect_button; } }

        private void connect_button_Click_1(object sender, EventArgs e)
        {
            if (!connected)
            {
                SetToStart();
                connected = connection.Connect(this, "127.0.0.1", 4444);
                if (connected)
                {
                    entry_button.Visible = false;
                    entry_button.Enabled = false;
                    nameLabel.Visible = true;
                    patientNameTextBox.Enabled = true;
                    patientNameTextBox.Visible = true;
                    nameButton.Enabled = true;
                    nameButton.Visible = true;
                    connect_button.Text = "Disconnect";
                }
                else
                {
                    MessageBox.Show("Start the server first!");
                }
            }
            else
            {
                if (connection.CheckIsConnected())
                {
                    connection.Send(new ClientDisconnectPacket(connection.ClientID));
                    connection.Disconnect();
                }
                connected = false;
                SetToStart();
            }
        }
        public struct Seat
        {
            public Seat(PictureBox s, string p, Label l)
            {
                seat = s;
                seat.BackColor = Color.DarkGreen;
                patient = p;
                nameLabel = l;
            }
            public PictureBox seat;
            public string patient;
            public Label nameLabel;
        }
        public Seat[] seats = null;

        public Doctors(DoctorsConnection connection)
        {
            InitializeComponent();
            this.connection = connection;

            entrySeatsSemaphore = new Semaphore();
            allowedToWriteToBufferSemaphore = new Semaphore();
            exitSemaphore = new Semaphore();

            entryBuffer = new Buffer();
            exitBuffer = new Buffer();


            entryPanel = new EntryPanelThread(new Point(17, 50),
                                                140,panel1,
                                                Color.White,
                                                entrySeatsSemaphore,
                                                entryBuffer, entry_button,
                                                nameButton,
                                                patientNameTextBox,
                                                connection);

            waitPanel = new WaitingPanelThread(new Point(17, 130),
                                                150, panel6,
                                                Color.White,
                                                entrySeatsSemaphore,
                                                entryBuffer, connection,
                                                true, false, 24);

            exitPanel = new WaitingPanelThread(new Point(20, 6),
                                                150, panel3,
                                                Color.White,
                                                exitSemaphore,
                                                exitBuffer, connection,
                                                false, true, 25);

            seats = new Seat[5];
            
            PictureBox tempPB = null;
            Label tempLabel = null;
            for (int i = 0; i < seats.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        tempPB = Seat5;
                        tempLabel = label_seat5;
                        break;
                    case 1:
                        tempPB = Seat4;
                        tempLabel = label_seat4;
                        break;
                    case 2:
                        tempPB = Seat3;
                        tempLabel = label_seat3;
                        break;
                    case 3:
                        tempPB = Seat2;
                        tempLabel = label_seat2;
                        break;
                    case 4:
                        tempPB = Seat1;
                        tempLabel = label_seat1;
                        break;

                }
                seats[i] = new Seat(tempPB, "", tempLabel);
            }


            entryThread = new Thread(new ThreadStart(entryPanel.Start));
            takeSeatThread = new Thread(new ThreadStart(waitPanel.Start));
            exitThread = new Thread(new ThreadStart(exitPanel.Start));
            
            entryThread.Start();
            takeSeatThread.Start();
            exitThread.Start();

            Closing += new CancelEventHandler(this.Doctors_Closing);
        }
        private void nameButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(patientNameTextBox.Text))
            {
                MessageBox.Show("Enter you name first!");
                return;
            }
            else
            {
                entryPanel.setPointColour(Color.Purple);
                entry_button.Visible = true;
                entry_button.Enabled = true;
                entryPanel.SetPatientName(patientNameTextBox.Text);
                nameButton.Text = "Now press enter";
            }

        }
        private void Doctors_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                connection.Send(new ClientDisconnectPacket(connection.ClientID));
                connection.Disconnect();
                Environment.Exit(Environment.ExitCode);
            }
            catch (Exception error)
            {
                MessageBox.Show("Error: " + error.Message);
            }
        }

        public void SetToStart()
        {
            connect_button.Text = "Connect";
            entry_button.Visible = false;
            entry_button.Enabled = false;
            nameLabel.Visible = false;
            patientNameTextBox.Enabled = false;
            patientNameTextBox.Visible = false;
            nameButton.Enabled = false;
            nameButton.Visible = false;
            entryPanel.setPointColour(Color.White);
            entry_button.BackColor = Color.DarkGreen;
            patientNameTextBox.Text = String.Empty;
            EntryPanel.ZeroPatient();
            WaitPanel.ZeroPatient();
        }
    }
}