﻿using SharedPackets;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Assignment2
{
    public class EntryPanelThread
    {
        private Point             origin;
        private int               delay, y, x;
        private Panel             panel;
        private Color             colour;
        private Point             patient;
        private Semaphore         semaphore;
        private Buffer            buffer;
        private Button            entryButton, nameButton;
        private bool              locked = true;
        private DoctorsConnection con;
        private string            patientName, clientID;
        private TextBox           patientNameTextBox;

        public EntryPanelThread(Point origin,
                                int delay,
                                Panel panel,
                                Color colour,
                                Semaphore semaphore,
                                Buffer buffer,
                                Button entryButton,
                                Button nameButton,
                                TextBox patientName,
                                DoctorsConnection con)
        {
            this.origin = origin;
            this.delay = delay;
            this.panel = panel;
            this.colour = colour;
            this.patient = origin;
            this.panel.Paint += new PaintEventHandler(this.Paint);
            this.x = 0;
            this.y = -5;
            this.semaphore = semaphore;
            this.buffer = buffer;
            this.entryButton = entryButton;
            this.nameButton = nameButton;
            this.patientNameTextBox = patientName;
            this.con = con;
            this.entryButton.Click += new System.EventHandler(this.entry_button_Click);
            semaphore.Signal();
        }
        public void SetClientID(string clientID)
        {
            this.clientID = clientID;
        }

        public void SetPatientName(string patientName)
        {
            this.patientName = patientName;
        }

        public void setPointColour(Color color)
        {
            this.colour = color;
        }

        private void entry_button_Click(object sender, EventArgs e)
        {
            if (entryButton.BackColor == Color.DarkGreen)
            {
                nameButton.Text = "Done";
                locked = false;
                entryButton.BackColor = Color.Red; 
                patientNameTextBox.Text = String.Empty;
                lock (this)
                {
                    if (!locked)
                    {
                        Monitor.Pulse(this);
                    }
                }
            }
        }

        public void Start()
        {
            Color signal = Color.Black;
            Thread.Sleep(delay);

            while (true)
            {
                lock (this)
                {
                    while (locked)
                    {
                        Monitor.Wait(this);
                    }
                    locked = true;
                }

                for (int j = 1; j <= 10; j++)
                {
                    if (!con.CheckIsConnected())
                    {
                        break;
                    }
                    this.MovePatient(x, y);
                    Thread.Sleep(delay);
                    panel.Invalidate();
                }

                string[] temp_id_name_array = new string[2];
                temp_id_name_array[0] = clientID;
                temp_id_name_array[1] = patientName;
                if (con.Send(new AddToWaitListPacket(temp_id_name_array)))
                {   
                    //wait for the waiting thread's signal
                    semaphore.Wait();
                }
                else
                {
                    MessageBox.Show("Connect first!");
                }
            }
        }

        public void SemaphoreSignal()
        {
            semaphore.Signal();
            this.ZeroPatient();
            panel.Invalidate();
            entryButton.BackColor = Color.DarkGreen;
        }
        public void WriteToBuffer(string patientColour, string patientName, string incomingID)
        {
            buffer.Write(Color.Black, patientName, incomingID);
        }

        public void ZeroPatient()
        {
            patient.X = origin.X;
            patient.Y = origin.Y;
            panel.Invalidate();
        }

        private void MovePatient(int x, int y)
        {
            patient.X += x; patient.Y += y;
        }

        private delegate void PaintDelegate(object sender, PaintEventArgs e);
        private void Paint(object sender, PaintEventArgs e)
        {
            if (this.panel.InvokeRequired)
            {
                this.panel.Invoke(new PaintDelegate(Paint), new object[] { sender, e });
            }
            else
            {
                Graphics g = e.Graphics;

                SolidBrush brush = new SolidBrush(colour);
                g.FillRectangle(brush, patient.X, patient.Y, 10, 10);
                brush.Dispose();
                g.Dispose();
            }
        }
    }
}

