﻿namespace Assignment2
{
    partial class Doctors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Doctors));
            this.connect_button = new System.Windows.Forms.Button();
            this.Seat1 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.entry_button = new System.Windows.Forms.Button();
            this.Seat2 = new System.Windows.Forms.PictureBox();
            this.Seat3 = new System.Windows.Forms.PictureBox();
            this.Seat4 = new System.Windows.Forms.PictureBox();
            this.Seat5 = new System.Windows.Forms.PictureBox();
            this.patientNameTextBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameButton = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label_seat1 = new System.Windows.Forms.Label();
            this.label_seat2 = new System.Windows.Forms.Label();
            this.label_seat3 = new System.Windows.Forms.Label();
            this.label_seat4 = new System.Windows.Forms.Label();
            this.label_seat5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Seat1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Seat2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seat3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seat4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seat5)).BeginInit();
            this.SuspendLayout();
            // 
            // connect_button
            // 
            this.connect_button.AutoSize = true;
            this.connect_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connect_button.Location = new System.Drawing.Point(34, 13);
            this.connect_button.Margin = new System.Windows.Forms.Padding(4);
            this.connect_button.Name = "connect_button";
            this.connect_button.Size = new System.Drawing.Size(108, 47);
            this.connect_button.TabIndex = 34;
            this.connect_button.Text = "Connect";
            this.connect_button.UseVisualStyleBackColor = true;
            this.connect_button.Click += new System.EventHandler(this.connect_button_Click_1);
            // 
            // Seat1
            // 
            this.Seat1.BackColor = System.Drawing.SystemColors.Control;
            this.Seat1.Image = ((System.Drawing.Image)(resources.GetObject("Seat1.Image")));
            this.Seat1.Location = new System.Drawing.Point(217, 241);
            this.Seat1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Seat1.Name = "Seat1";
            this.Seat1.Size = new System.Drawing.Size(129, 124);
            this.Seat1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Seat1.TabIndex = 24;
            this.Seat1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Location = new System.Drawing.Point(83, 369);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(62, 173);
            this.panel6.TabIndex = 23;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(25, 65);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(171, 300);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1008, 65);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(165, 300);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Controls.Add(this.entry_button);
            this.panel1.Location = new System.Drawing.Point(83, 548);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(63, 124);
            this.panel1.TabIndex = 35;
            // 
            // entry_button
            // 
            this.entry_button.BackColor = System.Drawing.Color.DarkGreen;
            this.entry_button.Enabled = false;
            this.entry_button.Location = new System.Drawing.Point(1, 76);
            this.entry_button.Margin = new System.Windows.Forms.Padding(4);
            this.entry_button.Name = "entry_button";
            this.entry_button.Size = new System.Drawing.Size(61, 47);
            this.entry_button.TabIndex = 38;
            this.entry_button.Text = "Enter";
            this.entry_button.UseVisualStyleBackColor = false;
            this.entry_button.Visible = false;
            // 
            // Seat2
            // 
            this.Seat2.BackColor = System.Drawing.SystemColors.Control;
            this.Seat2.Image = ((System.Drawing.Image)(resources.GetObject("Seat2.Image")));
            this.Seat2.Location = new System.Drawing.Point(370, 241);
            this.Seat2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Seat2.Name = "Seat2";
            this.Seat2.Size = new System.Drawing.Size(129, 124);
            this.Seat2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Seat2.TabIndex = 37;
            this.Seat2.TabStop = false;
            // 
            // Seat3
            // 
            this.Seat3.BackColor = System.Drawing.SystemColors.Control;
            this.Seat3.Image = ((System.Drawing.Image)(resources.GetObject("Seat3.Image")));
            this.Seat3.Location = new System.Drawing.Point(526, 241);
            this.Seat3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Seat3.Name = "Seat3";
            this.Seat3.Size = new System.Drawing.Size(129, 124);
            this.Seat3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Seat3.TabIndex = 38;
            this.Seat3.TabStop = false;
            // 
            // Seat4
            // 
            this.Seat4.BackColor = System.Drawing.SystemColors.Control;
            this.Seat4.Image = ((System.Drawing.Image)(resources.GetObject("Seat4.Image")));
            this.Seat4.Location = new System.Drawing.Point(685, 241);
            this.Seat4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Seat4.Name = "Seat4";
            this.Seat4.Size = new System.Drawing.Size(129, 124);
            this.Seat4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Seat4.TabIndex = 39;
            this.Seat4.TabStop = false;
            // 
            // Seat5
            // 
            this.Seat5.BackColor = System.Drawing.SystemColors.Control;
            this.Seat5.Image = ((System.Drawing.Image)(resources.GetObject("Seat5.Image")));
            this.Seat5.Location = new System.Drawing.Point(842, 241);
            this.Seat5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Seat5.Name = "Seat5";
            this.Seat5.Size = new System.Drawing.Size(129, 124);
            this.Seat5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Seat5.TabIndex = 40;
            this.Seat5.TabStop = false;
            // 
            // patientNameTextBox
            // 
            this.patientNameTextBox.Enabled = false;
            this.patientNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientNameTextBox.Location = new System.Drawing.Point(34, 731);
            this.patientNameTextBox.Name = "patientNameTextBox";
            this.patientNameTextBox.Size = new System.Drawing.Size(138, 26);
            this.patientNameTextBox.TabIndex = 41;
            this.patientNameTextBox.Visible = false;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(30, 694);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(142, 20);
            this.nameLabel.TabIndex = 42;
            this.nameLabel.Text = "Enter your name: ";
            this.nameLabel.Visible = false;
            // 
            // nameButton
            // 
            this.nameButton.AutoSize = true;
            this.nameButton.Enabled = false;
            this.nameButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameButton.Location = new System.Drawing.Point(178, 727);
            this.nameButton.Name = "nameButton";
            this.nameButton.Size = new System.Drawing.Size(68, 30);
            this.nameButton.TabIndex = 43;
            this.nameButton.Text = "Done";
            this.nameButton.UseVisualStyleBackColor = true;
            this.nameButton.Visible = false;
            this.nameButton.Click += new System.EventHandler(this.nameButton_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(1059, 370);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(71, 302);
            this.panel3.TabIndex = 45;
            // 
            // label_seat1
            // 
            this.label_seat1.AutoSize = true;
            this.label_seat1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_seat1.Location = new System.Drawing.Point(213, 373);
            this.label_seat1.Name = "label_seat1";
            this.label_seat1.Size = new System.Drawing.Size(0, 20);
            this.label_seat1.TabIndex = 46;
            this.label_seat1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_seat2
            // 
            this.label_seat2.AutoSize = true;
            this.label_seat2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_seat2.Location = new System.Drawing.Point(366, 373);
            this.label_seat2.Name = "label_seat2";
            this.label_seat2.Size = new System.Drawing.Size(0, 20);
            this.label_seat2.TabIndex = 47;
            this.label_seat2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_seat3
            // 
            this.label_seat3.AutoSize = true;
            this.label_seat3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_seat3.Location = new System.Drawing.Point(522, 373);
            this.label_seat3.Name = "label_seat3";
            this.label_seat3.Size = new System.Drawing.Size(0, 20);
            this.label_seat3.TabIndex = 48;
            this.label_seat3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_seat4
            // 
            this.label_seat4.AutoSize = true;
            this.label_seat4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_seat4.Location = new System.Drawing.Point(681, 373);
            this.label_seat4.Name = "label_seat4";
            this.label_seat4.Size = new System.Drawing.Size(0, 20);
            this.label_seat4.TabIndex = 49;
            this.label_seat4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_seat5
            // 
            this.label_seat5.AutoSize = true;
            this.label_seat5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_seat5.Location = new System.Drawing.Point(838, 373);
            this.label_seat5.Name = "label_seat5";
            this.label_seat5.Size = new System.Drawing.Size(0, 20);
            this.label_seat5.TabIndex = 50;
            this.label_seat5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Doctors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1268, 776);
            this.Controls.Add(this.label_seat5);
            this.Controls.Add(this.label_seat4);
            this.Controls.Add(this.label_seat3);
            this.Controls.Add(this.label_seat2);
            this.Controls.Add(this.label_seat1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.nameButton);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.patientNameTextBox);
            this.Controls.Add(this.Seat5);
            this.Controls.Add(this.Seat4);
            this.Controls.Add(this.Seat3);
            this.Controls.Add(this.Seat2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.connect_button);
            this.Controls.Add(this.Seat1);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Doctors";
            this.Text = "Staffordshire Doctors";
            ((System.ComponentModel.ISupportInitialize)(this.Seat1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Seat2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seat3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seat4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seat5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connect_button;
        private System.Windows.Forms.PictureBox Seat1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button entry_button;
        private System.Windows.Forms.PictureBox Seat2;
        private System.Windows.Forms.PictureBox Seat3;
        private System.Windows.Forms.PictureBox Seat4;
        private System.Windows.Forms.PictureBox Seat5;
        private System.Windows.Forms.TextBox patientNameTextBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Button nameButton;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label_seat1;
        private System.Windows.Forms.Label label_seat2;
        private System.Windows.Forms.Label label_seat3;
        private System.Windows.Forms.Label label_seat4;
        private System.Windows.Forms.Label label_seat5;
    }
}

