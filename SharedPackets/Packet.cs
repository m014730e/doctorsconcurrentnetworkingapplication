﻿using System;

namespace SharedPackets
{
    public enum PacketType
    {
        EMPTY,
        CLIENT_ID,
        ADD_TO_WAITLIST,
        ENTRY_PANEL_SIGNAL,
        MOVE_TO_WAIT_PANEL,
        REQUEST_SEAT,
        DECLINED_SEAT,
        SUCCESS_SIT,
        TAKEN_APPOINT,
        TRY_AGAIN,
        LEAVE,
        CLIENT_DISCONNECT,
        TRANSFER_CLIENT_ID,
        SERVER_DISCONNECT
    }
    [Serializable]
    public class Packet
    {
        public PacketType type = PacketType.EMPTY;
    }
    
    [Serializable]
    public class ClientIDPacket : Packet
    {
        public string client_ID;
        public string[] patientNames;
        public ClientIDPacket(string client_ID, string[] patientNames)
        {
            this.client_ID = client_ID;
            this.patientNames = patientNames;
            this.type = PacketType.CLIENT_ID;
        }
    }

    [Serializable]
    public class AddToWaitListPacket : Packet
    {
        public string[] id_name;
        public AddToWaitListPacket(string[] id_name)
        {
            this.id_name = id_name;
            this.type = PacketType.ADD_TO_WAITLIST;
        }
    }
    
    [Serializable]
    public class EntryPanelSignalPacket : Packet
    {
        public EntryPanelSignalPacket()
        {
            this.type = PacketType.ENTRY_PANEL_SIGNAL;
        }
    }

    [Serializable]
    public class MoveToWaitPanelPacket : Packet
    {
        public string[] id_name;
        public MoveToWaitPanelPacket(string[] id_name)
        {
            this.type = PacketType.MOVE_TO_WAIT_PANEL;
            this.id_name = id_name;
        }
    }

    [Serializable]
    public class RequestSeatPacket : Packet
    {
        public string patient_Colour;
        public string patientName;
        public string client_ID;
        public RequestSeatPacket(string patient_Colour, string patientName, string client_ID)
        {
            this.type = PacketType.REQUEST_SEAT;
            this.patient_Colour = patient_Colour;
            this.patientName = patientName;
            this.client_ID = client_ID;
        }
    }

    [Serializable]
    public class DeclinedSeatPacket : Packet
    {
        public string patient_Colour;
        public string patientName;
        public string client_ID;
        public DeclinedSeatPacket(string patient_Colour, string patientName, string client_ID)
        {
            this.type = PacketType.DECLINED_SEAT;
            this.patient_Colour = patient_Colour;
            this.patientName = patientName;
            this.client_ID = client_ID;
        }
    }

    [Serializable]
    public class SuccessSitPacket : Packet
    {
        public string[] patientNames;
        public SuccessSitPacket(string[] patientNames)
        {
            this.type = PacketType.SUCCESS_SIT;
            this.patientNames = patientNames;
        }
    }

    [Serializable]
    public class TakenAppointPacket : Packet
    {
        public string[] patientNames;
        public TakenAppointPacket(string[] patientNames)
        {
            this.type = PacketType.TAKEN_APPOINT;
            this.patientNames = patientNames;
        }
    }

    [Serializable]
    public class TryAgainPacket : Packet
    {
        public TryAgainPacket()
        {
            this.type = PacketType.TRY_AGAIN;
        }
    }

    [Serializable]
    public class LeavePacket : Packet
    {
        public string patient_Colour;
        public string patientName;
        public LeavePacket(string patient_Colour, string patientName)
        {
            this.type = PacketType.LEAVE;
            this.patient_Colour = patient_Colour;
            this.patientName = patientName;
        }
    }

    [Serializable]
    public class ClientDisconnectPacket : Packet
    {
        public string client_ID;
        public ClientDisconnectPacket(string client_ID)
        {
            this.type = PacketType.CLIENT_DISCONNECT;

            this.client_ID = client_ID;
        }
    }
    [Serializable]
    public class TransferClientIDPacket : Packet
    {
        public string leaving_client_ID;
        public string new_tranfser_client_ID;
        public TransferClientIDPacket(string leaving_client_ID, string new_tranfser_client_ID)
        {
            this.type = PacketType.TRANSFER_CLIENT_ID;
            this.leaving_client_ID = leaving_client_ID;
            this.new_tranfser_client_ID = new_tranfser_client_ID;
        }
    }
    [Serializable]
    public class ServerDisconnectPacket : Packet
    {
        public ServerDisconnectPacket()
        {
            this.type = PacketType.SERVER_DISCONNECT;
        }
    }
}
