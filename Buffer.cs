﻿using System.Threading;
using System.Drawing;

namespace Assignment2
{
    public class Buffer
    {
        private string patientName;
        private Color patientColour;
        private string incomingID;
        private bool empty = true;

        public void Read(ref Color patientColour, ref string patientName, ref string incomingID)
        {
            //Check whether buffer is empty
            lock (this)
            {
                if (empty)
                {
                    Monitor.Wait(this);
                }
                empty = true;
                patientName = this.patientName;
                patientColour = this.patientColour;
                incomingID = this.incomingID;
                Monitor.Pulse(this);
            }
        }

        public void Write(Color patientColour, string patientName, string incomingID)
        {
            //check if buffer is full
            lock (this)
            {
                if (!empty)
                {
                    Monitor.Wait(this);
                }
                empty = false;
                this.patientName = patientName;
                this.patientColour = patientColour;
                this.incomingID = incomingID;
                Monitor.Pulse(this);
            }
        }
    }
}
