﻿using SharedPackets;
using System;
using System.Drawing;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;

namespace Assignment2
{
    public class DoctorsConnection
    {
        private Doctors       form;
        private TcpClient     tcpClient;
        private NetworkStream stream;
        public  BinaryReader  reader;
        public  BinaryWriter  writer;
        private Thread        thread;
        private string        clientID;
        public  string        ClientID { get { return this.clientID; } }

        public bool Connect(Doctors form, string hostName, int port)
        {
            try
            {
                this.form = form;
                tcpClient = new TcpClient();
                tcpClient.Connect(hostName, port);
                stream = tcpClient.GetStream();
                this.reader = new BinaryReader(stream, Encoding.UTF8);
                this.writer = new BinaryWriter(stream, Encoding.UTF8);
                thread = new Thread(new ThreadStart(ProcessServerResponse));
                thread.Start();
            }
            catch (SocketException notConnected)
            {
                Console.WriteLine("You haven't opened the server, please open it" + notConnected.Message);
                return false;
            }
            return true;
        }
        public void Disconnect()
        {
            try
            {
                reader.Close();
                writer.Close();
                tcpClient.Close();
                thread.Abort();
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot disconnect");
            }
            finally
            {
                Console.WriteLine("Disconnected");
            } 
        }

        public bool CheckIsConnected()
        {
            return tcpClient.Connected;
        }

        public bool Send(Packet data)
        {
            try
            {
                if (this.tcpClient.Connected)
                {
                    MemoryStream memory = new MemoryStream();
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(memory, data);
                    byte[] buffer = memory.GetBuffer();

                    writer.Write(buffer.Length);
                    writer.Write(buffer);
                    writer.Flush();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
        private void ProcessServerResponse()
        {
            BinaryFormatter bf = new BinaryFormatter();
            int noOfIncomingBytes;
            try
            {
                while ((noOfIncomingBytes = reader.ReadInt32()) != 0)
                {
                    byte[] bytes = reader.ReadBytes(noOfIncomingBytes);
                    MemoryStream memory = new MemoryStream(bytes);
                    Packet packet = bf.Deserialize(memory) as Packet;

                    string colour, name, incomingId;
                    string[] patientNames;

                    switch (packet.type)
                    {
                        case PacketType.MOVE_TO_WAIT_PANEL:
                            incomingId = ((MoveToWaitPanelPacket)packet).id_name[0];
                            name = ((MoveToWaitPanelPacket)packet).id_name[1];
                            this.form.EntryPanel.WriteToBuffer("black", name, incomingId);
                            break;

                        case PacketType.ENTRY_PANEL_SIGNAL:
                            form.EntryPanel.SemaphoreSignal();
                            break;

                        case PacketType.CLIENT_ID:
                            clientID = ((ClientIDPacket)packet).client_ID;
                            patientNames = ((ClientIDPacket)packet).patientNames;
                            UpdateSeats(patientNames);
                            form.EntryPanel.SetClientID(clientID);
                            form.WaitPanel.SetClientID(clientID);
                            break;

                        case PacketType.SUCCESS_SIT:
                            patientNames = ((SuccessSitPacket)packet).patientNames;
                            UpdateSeats(patientNames);
                            this.form.WaitPanel.TakeSeat();
                            break;

                        case PacketType.TAKEN_APPOINT:
                            patientNames = ((TakenAppointPacket)packet).patientNames;
                            UpdateSeats(patientNames);
                            break;

                        case PacketType.TRY_AGAIN:
                            this.form.WaitPanel.TryAgainRequest();
                            break;

                        case PacketType.LEAVE:
                            colour = ((LeavePacket)packet).patient_Colour;
                            name = ((LeavePacket)packet).patientName;
                            this.form.ExitPanel.WriteToBuffer(colour, name, null);
                            break;

                        case PacketType.TRANSFER_CLIENT_ID:
                            this.form.WaitPanel.TransferClientID(((TransferClientIDPacket)packet).leaving_client_ID, ((TransferClientIDPacket)packet).new_tranfser_client_ID);
                            break;

                        case PacketType.SERVER_DISCONNECT:
                            this.form.WaitPanel.ZeroPatient();
                            this.form.EntryPanel.ZeroPatient();
                            this.form.Connection_Button.Text = "Connect";
                            Disconnect();
                           
                            break;
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        private delegate void UpdateSeatsDelegate(string[] patientNames);
        private void UpdateSeats(string[] patientNames)
        {
            if (form.InvokeRequired)
            {
                form.Invoke(new UpdateSeatsDelegate(UpdateSeats), new object[] { patientNames });
            }
            else
            {
                for (int patientIndex = 0; patientIndex < patientNames.Length; patientIndex++)
                {
                    if (String.IsNullOrEmpty(patientNames[patientIndex]))
                    {
                        form.seats[patientIndex].seat.BackColor = Color.DarkGreen;
                        form.seats[patientIndex].nameLabel.Text = String.Empty;
                    }
                    else
                    {
                        form.seats[patientIndex].seat.BackColor = Color.Red;
                        form.seats[patientIndex].nameLabel.Text = patientNames[patientIndex];
                    }
                }
            }
        }
    }       
}
