﻿using SharedPackets;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Assignment2
{
    public class WaitingPanelThread
    {
        private Point               origin;
        private int                 delay, y, x, loopNumber;
        private Panel               panel;
        private Color               colour;
        private Point               patient;
        private Semaphore           semaphore;
        private Buffer              buffer;
        private DoctorsConnection   con;
        private string              patientName, incomingPatientName, incomingPatientID, clientID;
        private bool                isTakeSeatPanel,isExitPanel;
        public  Semaphore           Semaphore { get { return this.semaphore; }}

        public WaitingPanelThread(  Point origin,
                                    int delay,
                                    Panel panel,
                                    Color colour,
                                    Semaphore semaphore,
                                    Buffer buffer, 
                                    DoctorsConnection con,
                                    bool isTakeSeatPanel,
                                    bool isExitPanel,
                                    int loopNumber)
        {
            this.origin = origin;
            this.delay = delay;
            this.panel = panel;
            this.colour = colour;
            this.patient = origin;
            this.panel.Paint += new PaintEventHandler(this.Paint);
            if (isTakeSeatPanel)
            {
                this.y = -5;
            }
            else
            {
                this.y = 10;
            }
            this.x = 0;
            this.semaphore = semaphore;
            this.buffer = buffer;
            this.con = con;
            this.isTakeSeatPanel = isTakeSeatPanel;
            this.isExitPanel = isExitPanel;
            this.loopNumber = loopNumber;
        }
        public void SetClientID(string clientID)
        {
            this.clientID = clientID;
        }

        public void SetPatientName(string patientName)
        {
            this.patientName = patientName;
        }

        public void Start()
        {
            while (true)
            {
                buffer.Read(ref this.colour, ref this.incomingPatientName, ref this.incomingPatientID);
                for (int j = 1; j <= loopNumber; j++)
                {
                    if (!con.CheckIsConnected())
                    {
                        break;
                    }
                    panel.Invalidate();
                    this.MovePatient(x, y);
                    Thread.Sleep(delay);
                }

                if (isTakeSeatPanel)
                {
                    if (this.incomingPatientID.Equals(this.clientID))
                    {
                        RequestSeatPacket takeSeatPacket = new RequestSeatPacket("black", this.incomingPatientName, this.clientID);
                        if (con.Send(takeSeatPacket))
                        {
                            semaphore.Wait();
                        }
                        else
                        {
                            MessageBox.Show("Connect first!");
                        }
                    }
                }
                else
                {
                    this.colour = Color.White;
                    this.ZeroPatient();
                    panel.Invalidate();
                }
           }
        }
        public void TakeSeat()
        {
            semaphore.Signal();
            this.colour = Color.White;
            this.ZeroPatient();
            panel.Invalidate();
        }
        public void TryAgainRequest()
        {
            string incoming_id = this.incomingPatientID;
            string current_id = this.clientID;
            if (this.incomingPatientID.Equals(this.clientID))
            {
                RequestSeatPacket takeSeatPacket = new RequestSeatPacket("black", this.incomingPatientName, this.clientID);
                if (con.Send(takeSeatPacket))
                {
                    //to show one person leaving and another taking a seat
                    Thread.Sleep(500);
                }
                else
                {
                    MessageBox.Show("Connect first!");
                }
            }
        }


        public void TransferClientID(string leavingClientID, string newTransferClientId)
        {
            if (this.incomingPatientID.Equals(leavingClientID))
            {
                incomingPatientID = newTransferClientId;
            }
        }
        public void ZeroPatient()
        {
            patient.X = origin.X;
            patient.Y = origin.Y;
            this.colour = Color.White;
            panel.Invalidate();
        }

        private void MovePatient(int x, int y)
        {
            patient.X += x; patient.Y += y;
        }
        private delegate void PaintDelegate(object sender, PaintEventArgs e);
        private void Paint(object sender, PaintEventArgs e)
        {
            if (this.panel.InvokeRequired)
            {
                this.panel.Invoke(new PaintDelegate(Paint), new object[] { sender, e });
            }
            else
            {
                Graphics g = e.Graphics;

                SolidBrush brush = new SolidBrush(colour);
                g.FillRectangle(brush, patient.X, patient.Y, 10, 10);
                brush.Dispose();
                g.Dispose();
            }
        }
        
        public void WriteToBuffer(string patientColour, string patientName, string incomingID)
        {
            buffer.Write(Color.Black, patientName, incomingID);
        }
    }
}
